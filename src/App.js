import logo from "./logo.svg";
import "./App.css";

// import Ex_Glass from "./Glass/Ex_Glass";
import Ex_ShoesRedux from "./Ex_ShoesShopRedux/Ex_ShoesRedux";

function App() {
  return (
    <div className="App">
      {/* <Glass /> */}
      {/* <Ex_Shoes /> */}
      <Ex_ShoesRedux />
    </div>
  );
}

export default App;
