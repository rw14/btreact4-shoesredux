import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoes from "./ItemShoes";
// import Shoe_Item from "./Shoe_Item";

class ListShoes extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.props.data.map((item, index) => {
            return (
              <div key={index} className="col-3">
                <ItemShoes item={item} />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    data: state.shoeReducer.shoeList,
  };
};

export default connect(mapStateToProps)(ListShoes);
