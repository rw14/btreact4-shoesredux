import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoes extends Component {
  render() {
    let { image, name, price, description } = this.props.shoeDetail;
    return (
      <div className="container p-5">
        <div className="row">
          <div className="col-4">
            <img src={image} alt="" className="w-100" />
          </div>
          <div className="col-8 text-left">
            <p>
              <span className="font-weight-bold">Name : </span> {name}
            </p>
            <p>
              <span className="font-weight-bold">Price :</span> {price} $
            </p>
            <p>
              <span className="font-weight-bold">Description :</span>{" "}
              {description}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeDetail: state.shoeReducer.shoeDetail,
  };
};

export default connect(mapStateToProps)(DetailShoes);
