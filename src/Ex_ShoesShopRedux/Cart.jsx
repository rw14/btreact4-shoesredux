import React, { Component } from "react";
import { connect } from "react-redux";
import {
  DECREASE_QUANTITY,
  DELETE_ITEM,
  INCREASE_QUANTITY,
} from "./redux/constant/shoeConstants.jsx";

class Cart extends Component {
  renderTbody = () => {
    return this.props.shoeCart.map((item) => {
      if (this.props.shoeCart.length == 0) {
        return <p>Vui long them san pham</p>;
      }
      return (
        <tr>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            {""}
            <img src={item.image} style={{ width: 80 }} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDecreaseQuantity(item);
              }}
              className="btn btn-warning"
            >
              -
            </button>
            <span className="mx-3">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.handleIncreaseQuantity(item);
              }}
              className="btn btn-danger"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDeleteFromCart(item);
              }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <table className="table table-dark">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Image</th>
              <th scope="col">Quantity</th>
              <th scope="col"></th>
              {/* <th scope="col">Total Price</th> */}
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
          <tfoot>
            <td colSpan="3"></td>
            <td>Total Price</td>
            <td>
              {this.props.shoeCart.reduce((tongTien, item, index) => {
                return (tongTien += item.soLuong * item.price);
              }, 0)}{" "}
              $
            </td>
          </tfoot>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeCart: state.shoeReducer.shoeCart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDecreaseQuantity: (value) => {
      dispatch({
        type: DECREASE_QUANTITY,
        payload: value,
      });
    },
    handleIncreaseQuantity: (value) => {
      dispatch({
        type: INCREASE_QUANTITY,
        payload: value,
      });
    },
    handleDeleteFromCart: (value) => {
      dispatch({
        type: DELETE_ITEM,
        payload: value,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
