import React, { Component } from "react";
import Cart from "./Cart.jsx";
import ListShoes from "./ListShoes.jsx";
import DetailShoes from "./DetailShoes.jsx";
export default class Ex_ShoesRedux extends Component {
  render() {
    return (
      <div>
        <Cart />
        <ListShoes />
        <DetailShoes />
      </div>
    );
  }
}
