import { dataShoe } from "../data_Shoe";
import {
  ADD_TO_CART,
  DECREASE_QUANTITY,
  DELETE_ITEM,
  INCREASE_QUANTITY,
  SHOW_DETAILS,
} from "../redux/constant/shoeConstants.jsx";

const initialState = {
  shoeList: dataShoe,
  shoeDetail: dataShoe[0],
  shoeCart: [],
};

export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SHOW_DETAILS: {
      state.shoeDetail = payload;
      return { ...state };
    }
    case ADD_TO_CART: {
      let cloneShoeCart = [...state.shoeCart];
      let index = state.shoeCart.findIndex((item) => {
        return item.id == payload.id;
      });

      if (index == -1) {
        let cartItem = { ...payload, soLuong: 1 };
        cloneShoeCart.push(cartItem);
      } else {
        cloneShoeCart[index].soLuong++;
      }
      state.shoeCart = cloneShoeCart;

      return { ...state };
    }
    case DECREASE_QUANTITY: {
      let cloneShoeCart = [...state.shoeCart];
      let index = state.shoeCart.findIndex((item) => {
        return item.id == payload.id;
      });
      if (cloneShoeCart[index].soLuong > 0) {
        cloneShoeCart[index].soLuong--;
      }
      state.shoeCart = cloneShoeCart;
      return { ...state };
    }
    case INCREASE_QUANTITY: {
      let cloneShoeCart = [...state.shoeCart];
      let index = state.shoeCart.findIndex((item) => {
        return item.id == payload.id;
      });
      cloneShoeCart[index].soLuong++;
      state.shoeCart = cloneShoeCart;
      return { ...state };
    }
    case DELETE_ITEM: {
      let cloneShoeCart = [...state.shoeCart];
      let index = state.shoeCart.findIndex((item) => {
        return item.id == payload.id;
      });

      if (index !== -1) {
        cloneShoeCart.splice(index - 1, 1);
      }
      state.shoeCart = cloneShoeCart;
      return { ...state };
    }
    default:
      return state;
  }
};
