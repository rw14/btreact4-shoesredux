import { combineReducers } from "redux";
import { shoeReducer } from "../redux/shoeReducer";

export const rootReducer_ShoeShop = combineReducers({
  shoeReducer,
});
